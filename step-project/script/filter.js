'use strict'


let btnServicecLoadMore = document.querySelector('.page-work .work-btn')
let btnListWorkFilt = document.querySelector('.work-filter');
let divLoad = document.createElement('div')

btnServicecLoadMore.addEventListener('click', showLoadingandLi);

btnListWorkFilt.addEventListener('click', function clickItemWork(event) {
    if (event.target.closest('.work-filter-item')) {
        let arrayItemWork = btnListWorkFilt.querySelectorAll('li');
        let activefilter = event.target.dataset.workitem;
        let arrayPortfolioWork = document.querySelectorAll('.page-work .portfolio-work li');
        [...arrayItemWork].map((element) => (activefilter === element.dataset.workitem)
            ? element.classList.add('filter-active') : element.classList.remove('filter-active'));

        if (activefilter === 'all') {
            [...arrayPortfolioWork].map((element) => element.classList.remove('portfolio-item-not-active'));
            reflectionbtnLoadMoreWork();
        } else {
            [...arrayPortfolioWork].map((element) => (activefilter === element.dataset.filter)
                ? element.classList.remove('portfolio-item-not-active') : element.classList.add('portfolio-item-not-active'));
            btnServicecLoadMore.classList.add('work-btn-no-active')
        }
    }

});

function showLoadingandLi() {
    setTimeout(showLoading, 0)
    setTimeout(removeLoading, 4000)
    setTimeout(showLiNewContent, 4100)
}


function showLiNewContent() {
    let arrayCatalog;
    let listWorkFilter = document.querySelectorAll('.work-filter li');
    let listPortfolioWork = document.querySelector('.portfolio-work');


    arrayCatalog = [...listWorkFilter].filter((elem) =>
        elem.hasAttribute('data-workitem') &&
        elem.getAttribute('data-workitem') !== 'all').map((elemAttr) => elemAttr.getAttribute('data-workitem'));

    arrayCatalog.map((element) => {
        for (let i = 0; i < 3; i++) {
            listPortfolioWork.append(createLiNewContentItem(element));
        }
    })

    reflectionbtnLoadMoreWork()
}

function createLiNewContentItem(filterItem) {
    let liContentServices, newLiContentServices, portfolioLiImgAttributeSrc, portfolioDescSubt;
    liContentServices = document.querySelector('.portfolio-item');
    newLiContentServices = liContentServices.cloneNode(true);

    portfolioLiImgAttributeSrc = newLiContentServices.querySelector('.portfolio-image');
    portfolioDescSubt = newLiContentServices.querySelector('.portfolio-description-subtitle');

    newLiContentServices.setAttribute('data-filter', `${filterItem}`);
    portfolioLiImgAttributeSrc.setAttribute('src', `./img/${filterItem}/${filterItem}${Math.floor(Math.random() * 10)}.jpg`);
    portfolioLiImgAttributeSrc.setAttribute('alt', `${filterItem}`)
    portfolioDescSubt.textContent = potrfDescSubtitle(filterItem);

    return newLiContentServices;
}

function potrfDescSubtitle(title) {
    if (title.includes('-')) {
        return `${title.split('-')[0].charAt(0).toUpperCase()}${title.split('-')[0].slice(1)} ${title.split('-')[1].charAt(0).toUpperCase()}${title.split('-')[1].slice(1)}`;
    } else {
        return `${title.charAt(0).toUpperCase()}${title.slice(1)}`;
    }
}

function reflectionbtnLoadMoreWork() {
    let arrayPortfolioWork = document.querySelectorAll('.page-work .portfolio-work li');
    if (arrayPortfolioWork.length < 36) {
        return btnServicecLoadMore.classList.remove('work-btn-no-active')
    } else {
        return btnServicecLoadMore.classList.add('work-btn-no-active')
    }
}

function showLoading() {
    btnServicecLoadMore.classList.add('load')
}


function removeLoading() {
    btnServicecLoadMore.classList.remove('load')
}


