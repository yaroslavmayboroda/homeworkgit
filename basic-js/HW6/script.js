'use strict'

//Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// відповідь екранування - відображати символи у тексті за допомогою спецсимвола (\). 
//Які засоби оголошення функцій ви знаєте?
// відповідь -  function declaration і function expresion 
//Що таке hoisting, як він працює для змінних та функцій?
// відповідь hoisting - (підняття)  переміщує всі оголошення на початок поточної області. 

const createNewUser = function () {

    const newUser = {
        getLogin() {
            return (`${this.firstName.charAt(0)}${this.lastName}`).toLowerCase();
        },
        setFirstName(value) {
            Object.defineProperty(this, 'firstName', {
                value: value
            })
        },
        setLastName(value) {
            Object.defineProperty(this, 'lastName', {
                value: value
            })
        },
        getAge() {
            let year = new Date(Date.parse(this.birthday.split('.').reverse().join(' ')));
            if (year.getFullYear() === new Date().getFullYear() - 1 && year.getMonth() >= new Date().getMonth() && year.getDate() > new Date().getDate()) {
                return 'згідно заповненої дати Вам менше 1 року'
            } else {
                return new Date().getFullYear() - year.getFullYear();
            }

        },
        getPassword() {
            return (`${this.firstName.charAt(0).toUpperCase()}${this.lastName.toLowerCase()}${new Date(Date.parse(this.birthday.split('.').reverse().join(' '))).getFullYear()}`);

        }

    }
    Object.defineProperties(newUser, {
        'firstName': {
            value: prompt('Enter name'),
            writable: false,
            configurable: true
        },
        'lastName': {
            value: prompt('Enter surname'),
            writable: false,
            configurable: true
        },
        'birthday': {
            value: prompt('Enter birthday day format \'dd.mm.yyyy\' ')
        }

    });

    return newUser
}


let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


