'use strict'
//Опишіть своїми словами як працює метод forEach.
//відповідь метод forEach-  бере масив в ньому перебирає кожен елемент і виконує сallback функцію
//Як очистити масив?
//відповідь array.lenght = 0
//Як можна перевірити, що та чи інша змінна є масивом?
//відповідь  array.isArray([]) = true 


function filterBy(array, type) {

    let newArray = [];
    if (type === 'null') {
        return array.filter(element => element !== null)
    } else {
        array.forEach((element) => {
            if ((type === 'object' && element === null) || typeof (element) !== type || Array.isArray(element)) {
                newArray.push(element)
            }
        });
        return newArray
    }
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

//additional control
//console.log(filterBy([23, 'tetx', {}, [], function () { }, true, false, undefined, null], 'object'));

