//Опишіть своїми словами що таке Document Object Model (DOM)
//відповідь об'єктна пмодуль документа, яка представляє весь зміст сторінки як об'єкти із якими маємо змогу маніпулювати (додавати /видаляти /змінювати )
//Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//Відповідь innerHTML - поверне весь вміст елемента включаючи теги і дочірні елементи із тегами  / innerText - поверне лише текстовий вмсіт елемента включаючи текстовий вміст дочірні елементів без інтервалів 
//Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//відповідь querySelector('class/tag/id') - універсальний метод як виконує пошук по всьому DOM згідно вказаному едлементу пошуку. На мою думку  даний спосіб кращий оскільки одним методом можна знайти любий елемент в DOM
//getElementById - пошук за id;
//getElementsByTagName - пошук за тегом






//task1
let parag = document.querySelectorAll('p')
parag.forEach((element => element.style.cssText = 'background:#ff0000;'))


//task2
console.log('task 2');
let isOptionList = document.getElementById('optionsList')
console.log(isOptionList);
console.log(isOptionList.parentElement);
console.log(isOptionList.childNodes);
isOptionList.childNodes.forEach((elem) => {
    console.log(`${elem.nodeName} - ${elem.nodeType}`);
})
console.log(' ');




//task 3
let testParag = document.querySelector('#testParagraph')
testParag.textContent = 'This is a paragraph'

//task4
console.log('task 4');
let mainHeader = document.querySelector('.main-header').children
console.log(mainHeader);
Array.from(mainHeader).forEach(elem => {
    elem.classList.add('nav-item')
})

//task 5 
let sectionTitle = document.querySelectorAll('.section-title')
sectionTitle.forEach((element => element.classList.remove('section-title')))

