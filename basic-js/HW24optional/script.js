// Написати реалізацію гри "Сапер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// створення контейнера 
let div = document.createElement('div')
div.classList.add('wrapper')
document.body.prepend(div)

// контейнер для інформації 
let divInfo = document.createElement('div')
divInfo.classList.add('wrapper-info')
div.prepend(divInfo)

// cтворення кнопки розпочати гру 
let btnNewGame = document.createElement('button')
btnNewGame.textContent = 'New Game'
btnNewGame.classList.add('btn-new-game', 'btn-hidden')
divInfo.prepend(btnNewGame)

//кількість мін 
let countBomb = document.createElement('p');
countBomb.classList.add('count-mine')
let imgBomb = document.createElement('img')
imgBomb.setAttribute('src', './img/mine.png')
imgBomb.setAttribute('alt', 'mine')
imgBomb.style.cssText = "width: 30px; height:30px"
let countBombSpan = document.createElement('span')
countBomb.append(imgBomb, countBombSpan)
divInfo.append(countBomb)

//кількість флагів 
let countFlag = document.createElement('p');
countFlag.classList.add('count-flag')
let imgFlag = document.createElement('img')
let countFlagSpan = document.createElement('span')
countFlagSpan.textContent = ' = 0'
imgFlag.setAttribute('src', './img/flag.png')
imgFlag.setAttribute('alt', 'flag')
imgFlag.style.cssText = "width: 30px; height:30px"
countFlag.append(imgFlag, countFlagSpan)
divInfo.append(countFlag)

// РЕЗУЛЬТАТ ГРИ 
let resultGame = document.createElement('p');
resultGame.textContent = 'You Lost';
resultGame.classList.add('result', 'result-hidden')
divInfo.append(resultGame);

// створення таблиц
let table = document.createElement('div');
table.classList.add('table');
let tableColumn = 8;
let tableColumnSquare = tableColumn * tableColumn;

createTable()

function createTable() {
    for (let i = 0; i < tableColumnSquare; i++) {
        let td = document.createElement('div');
        td.classList.add('td');
        td.setAttribute('data-id', `${i}`)
        table.append(td);
    }
    div.append(table);

    // масив  з числами  мін де будуть знаходитися в таблиці  
    let arrayNumberMine = [];
    while (arrayNumberMine.length < 10) {
        let num = Math.floor(Math.random() * (tableColumnSquare - 1));
        if (arrayNumberMine.includes(num)) continue;
        arrayNumberMine.push(num);
    }

    countBombSpan.textContent = ` = ${arrayNumberMine.length}`

    //створення тег img  міна додаємо на сторінку
    arrayNumberMine.forEach((element) => {
        let imgMine = document.createElement('img');
        imgMine.setAttribute('src', './img/mine.png');
        imgMine.setAttribute('alt', 'mine');
        imgMine.classList.add('img-mine', 'hidden')
        table.querySelector(`[data-id ="${element}"]`).classList.add('bomb')
        table.querySelector(`[data-id ="${element}"]`).append(imgMine)
    })
    countAboutBomb()
}

// функція яка повертає кількість рядом в бомб у клітинці
function countAboutBomb() {
    let tdItem = table.querySelectorAll('.td')

    for (let index of tdItem) {

        let count = 0
        i = +index.dataset.id
        let isLeft = (i % tableColumn === 0)
        let isRight = (i % tableColumn === tableColumn - 1);

        if (index.classList.contains('bomb')) continue;

        // перевіряє чи є бомба /вліво/ вгорі /вгорі*вліво /вгорі*вправо 
        if (i > 0 && !isLeft && tdItem[i - 1].classList.contains('bomb')) count += 1;
        if (i > tableColumn && tdItem[i - tableColumn].classList.contains('bomb')) count += 1;
        if (i > tableColumn - 1 && !isRight && tdItem[i + 1 - tableColumn].classList.contains('bomb')) count += 1;
        if (i > tableColumn + 1 && !isLeft && tdItem[i - 1 - tableColumn].classList.contains('bomb')) count += 1;

        // перевіряє чи є бомба низ/вправо /низ*ліво/низ*вправо
        if (i < tdItem.length - tableColumn && tdItem[i + tableColumn].classList.contains('bomb')) count += 1;
        if (i < tdItem.length - 1 && !isRight && tdItem[i + 1].classList.contains('bomb')) count += 1;
        if (i < tdItem.length + 1 - tableColumn && !isLeft && tdItem[i - 1 + tableColumn].classList.contains('bomb')) count += 1;
        if (i < tdItem.length - 1 - tableColumn && !isRight && tdItem[i + 1 + tableColumn].classList.contains('bomb')) count += 1;

        index.append(isNumberAboutMine(count))
    }

}
// додаємо колір цифрі
function isNumberAboutMine(num) {

    let span = document.createElement('span')

    if (num === 2) {
        span.classList.add('number', 'number-2', 'hidden')
    } else if (num >= 3) {
        span.classList.add('number', 'number-3', 'hidden')
    } else if (num === 1) {
        span.classList.add('number', 'hidden')
    } else {
        span.classList.add('num', 'hidden')
    }

    span.textContent = (num === 0) ? '' : num;
    return span
}


//автоматичне відкриття путих ячеєк

function openEmptyitem(element) {

    if (element === undefined) return
    if (element.firstElementChild.classList.contains('active')) return;
    if (element.firstElementChild.classList.contains("number")) {
        element.firstElementChild.classList.replace('hidden', 'active')
    } else {
        element.firstElementChild.classList.replace('hidden', 'active')

        let tdItem = table.querySelectorAll('.td')
        let index = +element.dataset.id

        let isLeft = index % tableColumn === 0
        let isRight = index % tableColumn === tableColumn - 1

        if (!isRight && tdItem[index + 1].firstElementChild.textContent === '') openEmptyitem(tdItem[index + 1])
        if (!isLeft && tdItem[index - 1].firstElementChild.textContent === '') openEmptyitem(tdItem[index - 1])
        if (index > tableColumn && tdItem[index - tableColumn].firstElementChild.textContent === '') openEmptyitem(tdItem[index - tableColumn])
        if (index < tdItem.length - tableColumn && tdItem[index + tableColumn].firstElementChild.textContent === '') openEmptyitem(tdItem[index + tableColumn])

        // число вправо
        if (!isRight && tdItem[index + 1].firstElementChild.classList.contains('number') && tdItem[index + 1].firstElementChild.classList.contains('hidden')) tdItem[index + 1].firstElementChild.classList.replace('hidden', 'active')
        //число вліво 
        if (!isLeft && tdItem[index - 1].firstElementChild.classList.contains('number') && tdItem[index - 1].firstElementChild.classList.contains('hidden')) tdItem[index - 1].firstElementChild.classList.replace('hidden', 'active')
        //чсило вгору
        if (index > tableColumn && tdItem[index - tableColumn].firstElementChild.classList.contains('number') && tdItem[index - tableColumn].firstElementChild.classList.contains('hidden')) tdItem[index - tableColumn].firstElementChild.classList.replace('hidden', 'active')
        //число вниз 
        if (index < tdItem.length - tableColumn && tdItem[index + tableColumn].firstElementChild.classList.contains('number') && tdItem[index + tableColumn].firstElementChild.classList.contains('hidden')) tdItem[index + tableColumn].firstElementChild.classList.replace('hidden', 'active')
        // число вгору вправо 
        if (index > tableColumn - 1 && !isRight && tdItem[index + 1 - tableColumn].firstElementChild.classList.contains('number') && tdItem[index + 1 - tableColumn].firstElementChild.classList.contains('hidden')) tdItem[index + 1 - tableColumn].firstElementChild.classList.replace('hidden', 'active')
        //число вгору вліво 
        if (index > tableColumn + 1 && !isLeft && tdItem[index - 1 - tableColumn].firstElementChild.classList.contains('number') && tdItem[index - 1 - tableColumn].firstElementChild.classList.contains('hidden')) tdItem[index - 1 - tableColumn].firstElementChild.classList.replace('hidden', 'active')
        // число вниз вліво
        if (index < tdItem.length + 1 - tableColumn && !isLeft && tdItem[index - 1 + tableColumn].firstElementChild.classList.contains('number') && tdItem[index - 1 + tableColumn].firstElementChild.classList.contains('hidden')) tdItem[index - 1 + tableColumn].firstElementChild.classList.replace('hidden', 'active')
        //число вниз вправо 
        if (index < tdItem.length - 1 - tableColumn && !isRight && tdItem[index + 1 + tableColumn].firstElementChild.classList.contains('number') && tdItem[index + 1 + tableColumn].firstElementChild.classList.contains('hidden')) tdItem[index + 1 + tableColumn].firstElementChild.classList.replace('hidden', 'active')
    }
}

// подія
table.addEventListener('click', playMineSapper)

function playMineSapper(event) {

    let target = event.target;

    // подія із прароцем 
    if (target.classList.contains('img-flag')) {
        targetParent = target.closest('.td')
        target.remove()
        target = targetParent
    }
    // кнопка нова гра  після першого натискання 
    btnNewGame.classList.replace('btn-hidden', 'btn-active')

    // путсий квадрат 
    if (target.closest('.td').querySelector('span')) {
        openEmptyitem(target)
    }
    //  якщо міна відсутніть дій
    if (table.querySelector('.img-mine').classList.contains('active')) {
        table.removeEventListener('click', playMineSapper)
    }
    //відобразити всі міни 
    if (target.closest('.td').querySelector('.img-mine')) {
        [...table.children].forEach((element) => {
            if (element.classList.contains('bomb')) element.firstElementChild.classList.replace('hidden', 'active')
        });
        // додаємо інфо програш
        resultGame.classList.add('lost')
        resultGame.classList.replace('result-hidden', 'result-active')
    }
    //  додаємо  інфо виграш 
    if (table.querySelectorAll('span.hidden').length === 0) {
        resultGame.textContent = 'You Won'
        resultGame.classList.add('won')
        resultGame.classList.replace('result-hidden', 'result-active')
    }
}

// подія на кнопку нова гра 
btnNewGame.addEventListener('click', function (event) {
    table.textContent = ''
    createTable();
    resultGame.classList.replace('result-active', 'result-hidden')
    resultGame.classList.remove('won', 'lost')
    table.addEventListener('click', playMineSapper)
})


// подія на поставлення флага 
table.addEventListener('contextmenu', function (event) {
    event.preventDefault()
    let target = event.target
    if (target.classList.contains('active')) return;
    if (target.classList.contains('img-flag')) {
        target.remove()
    }
    let imgFlag = document.createElement('img')
    imgFlag.setAttribute('src', './img/flag.png');
    imgFlag.setAttribute('alt', 'flag');
    imgFlag.classList.add('img-flag')
    target.prepend(imgFlag)
    countFlagSpan.textContent = ` = ${table.querySelectorAll('.img-flag').length}`

})

