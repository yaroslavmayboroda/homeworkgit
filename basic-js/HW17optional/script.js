'use strict'


//Створити порожній об'єкт student, з полями name та lastName.
//Запитати у користувача ім'я та прізвище студента, отримані значення 
//записати у відповідні поля об'єкта.
//У циклі запитувати у користувача назву предмета та оцінку щодо нього.

//Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл.

//Записати оцінки з усіх предметів як студента tabel.

//порахувати кількість поганих (менше 4) оцінок з предметів.
// Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
//Порахувати середній бал з предметів. 
//Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.

let student = {
    name: null,
    lastname: null
}
student.tabel = {};

const validZm = (zm) => (zm === '' || zm === null || !isNaN(zm)) ? true : false;

do {
    student.name = prompt('enter your name')
} while (validZm(student.name))
do {
    student.lastname = prompt('enter your lastname')
} while (validZm(student.lastname))


let subject, mark;

next: while (true) {
    subject = prompt('enter subject')
    if (subject === null) break
    if (subject !== '' && isNaN(subject)) {
        do {
            mark = prompt(`enter mark of the ${subject}`)
            student.tabel[subject] = (mark === null) ? null : +mark;
            if (mark === null) break next;
        } while (mark === '' || isNaN(mark) || mark < 1 || mark >= 13)
    }
}

student.nextCourses = function () {
    let arrayValue = Object.values(this.tabel);
    if (arrayValue.filter(elem => elem <= 4).length < 1) {
        alert(`Студент: ${this.name} ${this.lastname} переведено на наступний курс`)
        let avg = Math.ceil(arrayValue.reduce((acum, elem) => acum + elem, 0) / arrayValue.length)
        if (avg > 7) {
            alert(`Студенту ${this.name} ${this.lastname} призначено стипендію`);
        }
    } else {
        alert(`Студент: ${this.name} ${this.lastname} залишається на повторне вивчення  курсу`)

    }
}

student.nextCourses()



