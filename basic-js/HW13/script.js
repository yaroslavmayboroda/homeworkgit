// Теоретичні питання

// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

//відповідь : setTimeout() -  виконує один раз дію,  яка описана у функції через конкретний проміжок часу від запуску коду  і setInterval() - виконує дію безкінечно кількість разів, яка описана у функції  через конкретний інтервал часу .

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

// Відпоідь: Функція спрацює  після завантаження і обробки коду із затримкою в секундах та інтервал затримки залежить від факторів машини.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

//Відповідь setInterval() - виконує функцію без зупинки , якщо ввийшла затримка із даною функцією яку виконує setInterval() і вже не потрібна при виконання іншої функції , можна відмінити виконанння за допомогою clearInterval()


// Завдання
// Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.


'use strict'


let images = document.querySelectorAll('.image-to-show')

let div = document.createElement('div')
div.classList.add('button')
let playCycle = document.createElement('button')
playCycle.classList.add('btn-play')
playCycle.textContent = 'Відновити';
let stopCycle = document.createElement('button')
stopCycle.classList.add('btn-stop');
stopCycle.textContent = 'Припинити'
div.append(playCycle, stopCycle)


let timeId;
showImgCycle()
function showImgCycle(i = 0) {
    let index = i
    images[index].classList.add('active')
    document.body.append(div)
    timeId = setTimeout(function cycle() {
        images[index].classList.remove('active')
        index++
        if (index > images.length - 1) index = 0;
        images[index].classList.add('active')
        timeId = setTimeout(cycle, 3000)
    }, 3000)
}


stopCycle.addEventListener('click', () => {
    clearTimeout(timeId)
})

playCycle.addEventListener('click', function (e) {
    if (e.detail === 1) {
        let i = [...document.querySelectorAll('.image-to-show')].findIndex(e => e.classList.contains('active'))
        showImgCycle(i)
    } else {
        e.preventDefault()
    }
})






