const gulp = require('gulp');
const gulpClean = require('gulp-clean');
const concat = require('gulp-concat');
// css
const gulpScss = require('gulp-sass')(require('sass'));
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
// img
const gulpIMG = require('gulp-imagemin');
// js
const gulpUglify = require('gulp-uglify');

// server
const browserSync = require('browser-sync').create()


// build
//clean
gulp.task('clean', function (done) {
    gulp
        .src('./dist', { allowEmpty: true })
        .pipe(gulpClean())
        .on('finish', done);
});

//css
gulp.task('scss', function (done) {
    gulp
        .src('./src/scss/*.scss')
        .pipe(gulpScss().on('error', gulpScss.logError))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer({
            cascade: false,
        }))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./dist/css'))
        .on('end', done);
})

// js
gulp.task('uglifyJS', function (done) {
    gulp
        .src('./src/js/*.js')
        .pipe(concat('script.min.js'))
        .pipe(gulpUglify())
        .pipe(gulp.dest('./dist/js'))
        .on('end', done)

})
// img
gulp.task('imgmin', function (done) {
    gulp
        .src('./src/img/**/*.{png,jpg,jpeg,svg,gif,webp}')
        .pipe(gulpIMG())
        .pipe(gulp.dest('./dist/img/'))
        .on('end', done);
})


gulp.task('build', gulp.series('clean', 'scss', 'uglifyJS', 'imgmin'));

// dev



gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: './',
        },
        port: 3000,
        open: true
    })

    gulp.watch('*.html').on('change', browserSync.reload);
    gulp.watch('./src/scss/**/*.scss', gulp.series('scss')).on('change', browserSync.reload);
    gulp.watch('./src/js/*.js', gulp.series('uglifyJS')).on('change', browserSync.reload);
    gulp.watch('./src/img/**/*.{png,jpg,jpeg,svg,gif,webp}', gulp.series('imgmin')).on('change', browserSync.reload);
});

gulp.task('dev', gulp.series('build', 'server'))
